### Hi, I'm Luís - aka [NopeGuy][website] 👋



## Small stuff about me:
- 🎓 Hoping to finish graduation soon.
- 🔭 Always looking out for new ways to improve myself and how to code efficiently.
- 🌱 I’m currently learning C/Java/Python.


### Spotify Playing 🎧
[![Spotify](https://novatorem-pied-one.vercel.app/api/spotify)](https://open.spotify.com/user/11130361747)


## Projects 🗃

### C
-   [Computing Laboratories 3 (Parser and data management)](https://github.com/NopeGuy/LI3-2223)
-   [Operating Systems Project](https://github.com/NopeGuy/SO2122-G43)

### Java
-   [Best Text Based Football Manager Ever™](https://github.com/NopeGuy/project-poo-2021)

### Haskell
-   [BlockDude](https://github.com/NopeGuy/BlockDudeHaskell)


### Connect with me:


[![LinkedIn](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/lu%C3%ADs-ferreira-450413214/)
[![Twitter](https://img.shields.io/badge/Twitter-1DA1F2?style=for-the-badge&logo=twitter&logoColor=white)](https://twitter.com/GunaoDeJardim)

<br />


---

</details>


</details>

[website]: https://iamawesome.com/
[twitter]: https://twitter.com/GunaoDeJardim
[linkedin]: https://www.linkedin.com/in/lu%C3%ADs-ferreira-450413214/
